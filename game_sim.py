import os
import sys
import yaml
import random
import copy
import getopt

FAILURE_MULTIPLIER = 3
verbose = False

def debug_print(str):
	if verbose:
		print(str)


class Player:
	def __init__(self):
		self.turns = 1


class Step:
	def __init__(self, essential, recipe, text=""):
		self.essential = essential
		self.text = text
		self.fulfilled = False
		self.recipe = recipe


	def __str__(self):
		return "%s, Essential: %s, Fulfilled: %s" % (self.text, str(self.essential), str(self.fulfilled))


	def __repr__(self):
		return self.__str__()


	@classmethod
	def from_step_recipe_copy(cls, step, recipe):
		return cls(step.essential, recipe, step.text)


class Recipe:
	def __init__(self, name, misses_allowed, steps=dict()):
		self.name = name
		self.steps = steps
		self.current_step = 1
		self.misses_allowed = misses_allowed
		self.misses = 0

	def __str__(self):
		string = "Recipe: %s\nStep: %d/%d\nMiss: %d/%d\n" % (self.name, self.current_step, len(self.steps), self.misses, self.misses_allowed)
		for s in self.steps.keys():
			string += "Step %d: %s\n" % (s, str(self.steps[s]))
		return string


	def __repr__(self):
		return "Recipe: %s, Step: %d/%d, Miss: %d/%d\n" % (self.name, self.current_step, len(self.steps), self.misses, self.misses_allowed)


	# Make a deep copy of a recipe (e.g. for orders)
	@classmethod
	def from_recipe(cls, recipe):
		new_r = cls(recipe.name, recipe.misses_allowed)
		new_steps = dict()
		for i in recipe.steps.keys():
			new_steps[i] = Step.from_step_recipe_copy(recipe.steps[i], new_r)
		new_r.steps = new_steps
		return new_r


	def add_step(self, turn, essential, text=""):
		self.steps[turn] = Step(essential, self, text)


	def is_done(self):
		return self.current_step == len(self.steps)


	def is_failure(self):
		return self.misses >= self.misses_allowed


class Game:
	def __init__(self, recipes, players, miss_limit=10, day_length=50, day_limit=1):
		self.orders = list()
		self.recipes = recipes
		self.misses = 0
		self.day = 0
		self.turn = 0
		self.day_length = day_length
		self.day_limit = day_limit
		self.miss_limit = miss_limit
		self.players = players
		self.order_interval = 5

		# metrics
		self.wasted_turns = 0
		self.dishes_completed = 0
		self.dishes_failed = 0


	@classmethod
	def from_yaml(cls, yaml_input, num_players):
		recipes = []
		for i in yaml_input.keys():
			info = yaml_input[i]
			r = Recipe(i, info['misses-allowed'])
			for s in info['steps']:
				r.add_step(s['turn'], s['essential'], s['name'])
			recipes.append(r)

		players = []
		for i in range(0, num_players):
			players.append(Player())

		return cls(recipes, players)

	def is_lost(self):
		return (self.misses >= self.miss_limit)


	def is_won(self):
		return (self.day == self.day_limit)


	def complete_order(self, order):
		self.misses += order.misses
		debug_print("Order of %s completed! %d miss tokens added, (%d/%d)" % (order.name, order.misses, self.misses, self.miss_limit))
		self.orders.remove(order)
		self.dishes_completed += 1


	def fail_order(self, order):
		misses_added = order.misses_allowed * FAILURE_MULTIPLIER
		self.misses += misses_added
		debug_print("Order of %s FAILED! %d miss tokens added, (%d/%d)" % (order.name, misses_added, self.misses, self.miss_limit))
		self.orders.remove(order)
		self.dishes_failed += 1


	def add_order(self):
		r = self.recipes[random.randint(0, len(self.recipes) - 1)]
		new_order = Recipe.from_recipe(r)
		self.orders.append(new_order)
		debug_print("New order for %s just came in!" % (new_order.name))
		return new_order


	def fulfill_step(self, step):
		if step.essential:
			debug_print("Player carries out essential step %d in order for %s" % (step.recipe.current_step, step.recipe.name))
		else:
			debug_print("Player carries out step %d in order for %s" % (step.recipe.current_step, step.recipe.name))
		step.fulfilled = True
		if step.recipe.is_done():
			self.complete_order(step.recipe)
		else:
			step.recipe.current_step += 1


	def miss_step(self, step):
		# TODO: essential step handling
		if step.essential:
			debug_print("MISS: essential step %d in order for %s" % (step.recipe.current_step, step.recipe.name))
			self.fail_order(step.recipe)
		else:
			step.recipe.misses += 1
			debug_print("MISS: step %d in order for %s. Miss token added. (%d/%d)" % (step.recipe.current_step, step.recipe.name, step.recipe.misses, step.recipe.misses_allowed))
			if step.recipe.is_failure():
				self.fail_order(step.recipe)


	def print_recipes(self):
		for r in self.recipes:
			debug_print(r)


	def print_game_metrics(self):
		debug_print("END OF GAME METRICS")
		debug_print("===================")
		debug_print("Total misses: %d/%d" % (self.misses, self.miss_limit))
		debug_print("Wasted turns: %d" % self.wasted_turns)
		debug_print("Dishes completed: %d" % self.dishes_completed)
		debug_print("Dishes failed: %d" % self.dishes_failed)


	def get_game_metrics(self):
		return {'misses': self.misses, \
				'wasted_turns': self.wasted_turns, \
				'dishes_completed': self.dishes_completed, \
				'dishes_failed': self.dishes_failed}


def run_game(game):
	# Each loop is one turn
	order_timer = 0

	debug_print("Let the game begin!")
	debug_print("============")
	debug_print("DAY %d" % (game.day))
	debug_print("============")

	o = game.add_order()

	while not (game.is_won() or game.is_lost()):
		game.turn += 1
		players_left = len(game.players)
		debug_print("Turn #%d of %d, %d misses" % (game.turn, game.day_length, game.misses))

		if order_timer == game.order_interval or len(game.orders) == 0:
			o = game.add_order()
			debug_print("New order for %s just came in!" % (o.name))
			order_timer = 0
		else:
			order_timer += 1

		work_essential = list()
		for i in game.orders:
			if o.current_step in o.steps and o.steps[o.current_step].essential:
				work_essential.append(o.steps[o.current_step])

		d = dict(map(lambda i:(i.recipe.misses_allowed,i), work_essential))
		sorted_costs = sorted(d.keys())

		# TODO: Merge this with the general loop
		# If we must miss essential steps (fail a recipe) because we don't have 
		# enough people, choose the least painful ones to lose
		essential_fails = len(work_essential) - len(game.players)
		if essential_fails > 0:
			# TODO: FAIL EVERYTHING? MAKES NO SENSE.
			for i in range(0, players_left):
				game.fail_order(d[i].recipe)
			continue
		else:
			# Otherwise, expend players on all available essential steps
			for i in d.keys():
				game.fulfill_step(d[i])
				players_left -= 1

		# For the rest, if we still have players left and we haven't lost yet, 
		# work on orders starting from the closest to failure
		work = dict()
		for i in game.orders:
			if i.current_step in i.steps:
				failure_margin = i.misses_allowed - i.misses
				if failure_margin not in work:
					work[failure_margin] = []
				work[failure_margin].append(i.steps[i.current_step])

		for p in range(0, players_left):
			if len(work.keys()) > 0:
				next_key = min(work.keys())
				next_step = work[next_key].pop()
				game.fulfill_step(next_step)
				if len(work[next_key]) <= 0:
					del work[next_key]
			else:
				debug_print("Player %d has nothing to do!" % (p))
				game.wasted_turns += 1
				break

		# If there's work left to do that nobody got to, those get miss counters
		for margin in work.keys():
			for step in work[margin]:
				game.miss_step(step)
			del work[margin]

		if game.turn == game.day_length:
			game.turn = 0
			game.day += 1
			debug_print("============")
			debug_print("END OF DAY %d" % (game.day))
			debug_print("============")


	if game.is_won():
		debug_print("YOU WIN!")
	elif game.is_lost():
		debug_print("YOU LOSE!")

	game.print_game_metrics()


def usage():
	print("Usage: %s yaml-file-name [-i iterations] [-v | --verbose]" % sys.argv[0])


def indices(comp, cat):
	highest = comp(cat)
	highest_indices = []
	for i in range(0, len(cat)):
		if cat[i] == highest:
			highest_indices.append(i)

	return highest_indices


# could have used numpy, but I'm having a hard time installing that on Windows
def median(mylist):
    sorts = sorted(mylist)
    length = len(sorts)
    if not length % 2:
        return (sorts[length / 2] + sorts[length / 2 - 1]) / 2.0
    return sorts[length / 2]


def print_metric(cat_name, cat):
	print(cat_name)
	print("Lowest : %d" % (min(cat)))
	print("Highest: %d" % (max(cat)))
	print("Mean:    %f" % (float(sum(cat)) / len(cat)))
	print("Median:  %d" % (median(cat)))


def main():
	if len(sys.argv) < 2:
		usage()
		sys.exit()

	try:
		opts, args = getopt.getopt(sys.argv[2:], "i:v", ["verbose", "iterations="])
	except getopt.GetoptError as err:
		print str(err)
		usage()
		sys.exit(2)

	iterations = 1
	global verbose
	for o, a in opts:
		if o in ("-i", "--iterations"):
			iterations = int(a)
		elif o in ("-v", "--verbose"):
			verbose = True
		elif o in ("-h"):
			usage()
			sys.exit()

	metrics = {'wins': 0, 'losses': 0, 'misses': [], 'wasted_turns': [], 'dishes_completed': [], 'dishes_failed': []}

	f = file(sys.argv[1], 'r')
	yaml_input = yaml.load(f)

	for i in range(0, iterations):
		g = Game.from_yaml(yaml_input, 3)

		g.day_limit = 3
		# Need to change up how new orders come in. This is too deterministic.
		g.order_interval = 3

		run_game(g)

		if g.is_won():
			metrics['wins'] += 1
		else:
			metrics['losses'] += 1

		metrics['misses'].append(g.misses)
		metrics['wasted_turns'].append(g.wasted_turns)
		metrics['dishes_completed'].append(g.dishes_completed)
		metrics['dishes_failed'].append(g.dishes_failed)


	print("TEST RUN COMPLETE")
	print("===================")
	print("Games:  %d" % iterations)
	print("Wins:   %d" % metrics['wins'])
	print("Losses: %d" % metrics['losses'])
	print_metric("Misses: ", metrics['misses'])
	print_metric("Wasted Turns: ", metrics['wasted_turns'])
	print_metric("Dishes Completed: ", metrics['dishes_completed'])
	print_metric("Dishes Failed: ", metrics['dishes_failed'])
	


if __name__ == "__main__":
	main()