A short experiment on assisting game balance by running my card game prototype, Brigade de Cuisine, with AI players and evaluating win/lose metrics. 

Future work would have included having different basic play styles such as being greedier or more conservative to introduce some randomness.